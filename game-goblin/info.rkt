#lang info

(define deps '("base" "goblins"))
(define build-deps '("rackunit-lib" "scribble-lib" "sandbox-lib"))
(define pkg-desc
  "Easy integration of Goblins and Lux")
(define version "0.1")
(define pkg-authors '("cwebber"))
#;(define scribblings '(("scribblings/goblins.scrbl" (multi-page))))
